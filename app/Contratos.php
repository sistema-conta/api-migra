<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contratos extends Model
{
    protected $table = 'contrato';

    protected $primaryKey = 'con_id';

    public function buscarTodos()
    {

        $lista = [];

        $contratos = $this->where('con_situacao', 'A')->get();

        foreach ($contratos as $contrato) {
            $lista[] = $this->montaUrlId('contrato', $contrato->con_id);
        }

        return $lista;
    }


    private function montaUrlId($table = null, $id = null)
    {
        return url('/api/v1') . '/' . $table . '/' . $id . '?token=' . config('app.key');
    }
}
