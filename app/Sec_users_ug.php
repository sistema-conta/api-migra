<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sec_users_ug extends Model
{
    protected $table = 'sec_users_ug';

    protected $primaryKey = 'login';

}
