<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Codigo_item extends Model
{
    protected $table = 'codigo_item';

    protected $primaryKey = 'cit_id';
}
